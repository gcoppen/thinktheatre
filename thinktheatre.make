; Think Theatre make file
api = 2
core = 7.x

; Modules

projects[domain][version] = "3.6"
projects[domain][subdir] = "contrib"

projects[domaincontext][version] = "1.0-alpha1"
projects[domaincontext][subdir] = "contrib"

projects[flexslider][version] = "2.x-dev"
projects[flexslider][subdir] = "contrib"

projects[file_entity][version] = "2.0-unstable7"
projects[file_entity][subdir] = "contrib"

projects[fitvids][version] = "1.8"
projects[fitvids][subdir] = "contrib"

projects[media][version] = "2.x-dev"
projects[media][subdir] = "contrib"

projects[media_vimeo][version] = "2.x-dev"
projects[media_vimeo][subdir] = "contrib"

projects[media_youtube][version] = "2.0-rc1"
projects[media_youtube][subdir] = "contrib"

projects[modernizr][version] = "3.0-beta3"
projects[modernizr][subdir] = "contrib"

projects[zoundation][type] = "theme"
projects[zoundation][version] = "4.x-dev"
projects[zoundation][subdir] = "contrib"


; Themes
; cogito
projects[cogito][type] = "theme"
projects[cogito][version] = "1.0-alpha1"

; Patches
projects[media_youtube][patch][] = "http://drupal.org/files/media_youtube-mimetype-update-1812976-35.patch"

